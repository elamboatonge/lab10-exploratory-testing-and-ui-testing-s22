import time
from selenium import webdriver

driver = webdriver.Chrome('/chromedriver')
driver.get("https://vk.com/hockeyvezde")

username = driver.find_element_by_id("quick_email")
password = driver.find_element_by_id("quick_pass")
input_email = input("Enter email:")
input_pass = input("Enter password:")
username.send_keys(input_email)
password.send_keys(input_pass)
driver.find_element_by_id("quick_login_button").click()
time.sleep(3)
auth_code = driver.find_element_by_id("authcheck_code")
input_code = int(input("Enter auth code:"))
auth_code.send_keys(input_code)
driver.find_element_by_id("login_authcheck_submit_btn").click()

SCROLL_PAUSE_TIME = 2
# Get scroll height
last_height = driver.execute_script("return document.body.scrollHeight")
count = 0
while True:
    # Scroll down to bottom
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # Wait to load page
    time.sleep(SCROLL_PAUSE_TIME)

    # Calculate new scroll height and compare with last scroll height
    new_height = driver.execute_script("return document.body.scrollHeight")

    # To finally break
    # if new_height == last_height:
    #     break
    # last_height = new_height
    if count == 20:
        break
    count += 1


